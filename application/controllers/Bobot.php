<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Bobot extends CI_Controller
{
	public function get($tujuan)
	{
		if ($tujuan == 'individu') {
			$do = DB_CUSTOM::bobot('individu'); //data diambil berdasarkan variabel
		} else {
			$do = DB_CUSTOM::bobot('keluarga');
		}

		if (!$do->error) {//foreach= data diberi alias dengan variabel json 
			foreach ($do->data as $json) { 
				$json->kriteria = json_decode($json->kriteria);//di objek kriteria data didecode dikarenakan data dalam bentuk json
				foreach ($json->kriteria as $json_sub) { //dalam php untuk membaca data dengan baik dijadikan array termasuk dengan subkriteria
					$json_sub->sub_kriteria = json_decode($json_sub->sub_kriteria);
				}
			}
			success("data berhasil ditemukan", $do->data);
		} else
			error("data gagal ditemukan");
	}
}


/* End of file Bobot.php */
/* Location: ./application/controllers/Bobot.php */
