<?php

class Manajemen extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->session->has_userdata('logged_in')) {
			redirect('login');
		}
		if ($this->session->role !== 'admin')
			redirect('admin');
	}

	public function index()
	{
		// load view admin/overview.php
		$this->load->view("admin/manajemen");
	}
}
